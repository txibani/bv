module.exports = function(grunt) {
	grunt.initConfig({
        //Task configuration compile css
        compass: {                    // Task
            dev: {                    // Target
                options: {
                    sassDir: '../bv-fe-project/style/sass', // Sass file directory
                    cssDir: '../bv-fe-project/style/css'    // Css file directory
                }
            }
        },

        watch: {
		      'compass': {
		        files: ['../bv-fe-project/style/sass/*.scss'],     // Watch for any change at all sass files inside sass folder
		        tasks: ['compass']
		      }
      }

	});

    // These plugins provide necessary tasks.
        grunt.loadNpmTasks('grunt-contrib-compass');
        grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task.
//        grunt.registerTask('default', ['watch']);
    grunt.registerTask('default', ['compass', 'watch']);
};
