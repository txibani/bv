For this project I'll use SASS and Grunt.js.

Grunt task runner to compile SASS to CSS.
In the console run "grunt" to watch the sass file for any change and compile.

In folder grunt-task-runner you can find the gruntFile and the node module to compile.
In the folder bv-fe-project there are the main files of the project. I have divided the project
in different folders to keep it more readable.
In the style folder there are two more folder, one for all the scss files (will be compile into one
main.css file) and the other one that contains the main.css file that will be call from the index.html
